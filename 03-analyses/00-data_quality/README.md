## Description

Before any population structure analysis, raw genotyping data have to be checked

## Training Data
dataset_1: **chipseq70k_dataset_example_PopStruct**

---
## Details of analyses

### Step 1. check missing data

First extract missing stats with PLINK

```sh
plink -file 02-data/dataset_example_PopStruct -aec -missing -out 03-analyses/00-data_quality/dataset_example_PopStruct
```
Then plot proportion of missing data per sampling site and among SNPs using the Rscript cmd below :

```sh
Rscript 00-scripts/01-print_missing_figure.R 03-analyses/00-data_quality/ 04-results/01-figures/Proportion_of_missing_dataset_exemple_PopStruct
```
<img src="../../04-results/01 - figures/Missingness_dataset_example_PopStruct.png"  width="600">

**Note 1: As you can see, there is an extreme individual showing ~45% of missing data. Therefore, before any Population structure analysis, we have to exclude this one.

**Note 2: In the same idea, SNPs showing high level of missing data should be excluded.

---> PLINK allow filtering parameters for this : `-geno` for SNPs and `-mind` for individuals. See the PLINK 1.9 manual for more details.

### Step 2. Heterosigosity and Fis
