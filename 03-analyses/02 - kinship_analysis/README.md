## Description

Investigate Relatedness and Kinship among samples

## Training Data
dataset_2: **dataset_example_families**

---
## Details of analyses
Here we used the SNPRelate package to assess genetic relatedness as well as kinship stats among samples.

Use the R script `00-scripts/SNPRelate.R` to explore putative families

**Note: all analyses are based on the online tutorial for SNPRelate at https://bioconductor.org/packages/release/bioc/vignettes/SNPRelate/inst/doc/SNPRelate.html01 - figures



<img src="../../04-results/01 - figures/data_example_families_relatedness_K0_K1.png" width="400">

<img src="../../04-results/01 - figures/data_example_families_IBS_KING.png" width="400">

<img src="../../04-results/01 - figures/data_example_families_cluster_analysis.png" width="400">

<img src="../../04-results/01 - figures/data_example_families_MDS_IBS.png" width="400">

<img src="../../04-results/01 - figures/data_example_families_IBS_KING_grp.png" width="400">
