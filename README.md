## Description

This repository gathering all the working scripts used to get results from analyses of the 70k SNP-chip conducted to Pmarg Project.

#### Authors & Affiliations:
Yann Dorant <yann.dorant@ifremer.fr> (maintainer)

Alexandre Lambard <alexandre.lambard@ifremer.fr> (developper)

Jérémy Le Luyer <Jeremy.Le.Luyer@ifremer.fr> (developper)

## Repository Framework
* **00-scripts:** scripts for data analyses
* **01-info_files:** list of files used to get information about samples
* **02-data:** folder including raw data and example datasets
* **03-analyses:** folder including full descrption of genetic analyses
* **04-results:** folder including main results (Figures and Tables)

## Dependencies
1. **System** : Linux or MacOS

2. **Softwares**
  - Analysis power tool v2.11
  - PLINK v1.9
  - vcftools v0.1.16+
  - Admixture v1.3.0+
  - parallel for bash

3. **R libraries**
  * data treatment: Dplyr, magrittr, tibble, tidyr,reshape2
  * data plotting: ggplot2, patchwork, pophepler, RColorBrewer
  * Genomic analyses: adegenet, StAMPP, Outflank.V2, gdsfmt, SNPRelate, PCAdapt, assignPOP

4. **External Git repository**
  * Toolbox for genomic dataset conversion [https://gitlab.com/YDorant/Toolbox]

## Training Data
We included two example datasets for testing.

* dataset_1: **chipseq70k_dataset_example_PopStruct**, which contains 10,000 SNPs and 289 individuals originating from six sampling sites.

* dataset_2: **chipseq70k_dataset_example_families**, which contains 10,000 SNPs and 260 individuals originating from multiples unknown families.


## Conceptual Framework
<img src="01-infos-files/00-archives/diagram_gitlab.png"  width="400">
